package com.saeedalam.chimera.screens;

import java.util.ArrayList;
import java.util.Arrays;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.saeedalam.chimera.settings.Main;

public class ScreenManager {

	private static Game game = null;
	private static ArrayList<Screen> screens;
	
	public static void initialize (Game aGame) {
		game = aGame;

		screens = new ArrayList<Screen>();
		
		//instantiate all Screens here
		MenuScreen MS = new MenuScreen(aGame);
		CharacterSelect CS = new CharacterSelect(aGame);
		
		addScreens(MS, CS);
	}
	
	public static ArrayList<Screen> getScreens() {
		return screens;
	}
	
	public static void addScreens(Screen ... aScreens ) {
		screens.addAll(new ArrayList<Screen>(Arrays.asList(aScreens)));
	}
	
	public static Screen getScreen(String aName) {
		
		for(Screen screen : screens) {
			if(screen.getClass().getName().equals(aName)) {
				return screen;
			}
		}
		
		Main.LOG.println("Could not find the Screen '"+aName+"'. Make sure it is instantiated in ScreenManager. ");
		return null;
	}
	
	
}
