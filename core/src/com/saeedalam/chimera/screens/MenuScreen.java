package com.saeedalam.chimera.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.saeedalam.chimera.Chimera;

public class MenuScreen extends AbstractScreen {
	
	public MenuScreen(Game aGame) {
		super(aGame);
		
		Label title = new Label("WELCOME TO CHIMERA", Chimera.gameSkin);
	    title.setAlignment(Align.center);
	    title.setY(Gdx.graphics.getHeight()*2/3);
	    title.setWidth(Gdx.graphics.getWidth());
		
	    TextButton playButton = new TextButton("Play Game",Chimera.gameSkin);
	    playButton.setWidth(Gdx.graphics.getWidth()/2);
	    playButton.setPosition(Gdx.graphics.getWidth()/2-playButton.getWidth()/2,Gdx.graphics.getHeight()/2-playButton.getHeight()/2);
	    playButton.addListener(new InputListener(){
	    	@Override
	    	public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
//                getGame().setScreen(new CharacterSelect(getGame()));
	    		getGame().setScreen(ScreenManager.getScreen(CharacterSelect.class.getName()));
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
	    
	    this.addActor(playButton);
		this.addActor(title);
	}


	
}
