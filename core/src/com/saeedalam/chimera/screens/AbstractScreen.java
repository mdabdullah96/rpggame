package com.saeedalam.chimera.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.saeedalam.chimera.Chimera;

public class AbstractScreen extends Stage implements Screen{
	
	public static final String NAME = "AbstractScreen";
	
	private Texture background;
	private Game game;
	
	public AbstractScreen(Game aGame) {
		this.game = aGame;
		background = new Texture("background.png");
		
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		Gdx.input.setInputProcessor(this);		
		
	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		this.getBatch().begin();
		this.getBatch().draw(background, 0, 0, Chimera.WIDTH, Chimera.HEIGHT);		
		this.getBatch().end();
		this.act(Gdx.graphics.getDeltaTime());
		this.draw();
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		super.dispose();
		this.getBatch().dispose();
		this.dispose();
	}
	
	public Texture getBackground() {
		return background;
	}
	
	public void setBackground(Texture aBackground) {
		this.background = aBackground;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	
}
