package com.saeedalam.chimera.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.utils.Collision;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.saeedalam.chimera.Chimera;
import com.saeedalam.chimera.actions.Movement;
import com.saeedalam.chimera.settings.Main;

public class CharacterSelect extends AbstractScreen{
	
	public static final String NAME = "CharacterSelect";

	public CharacterSelect(Game aGame) {
		super(aGame);
		
		Label title = new Label("Choose your Hero!", Chimera.gameSkin);
	    title.setAlignment(Align.center);
	    title.setY(Gdx.graphics.getHeight()*2/3);
	    title.setWidth(Gdx.graphics.getWidth());
	    
	    TextButton playButton = new TextButton("PLAY",Chimera.gameSkin);
	    playButton.setWidth(Gdx.graphics.getWidth()/2);
	    playButton.setPosition(Gdx.graphics.getWidth()/2-playButton.getWidth()/2,Gdx.graphics.getHeight()/2-playButton.getHeight()/2);
	    playButton.addListener(new InputListener(){
	    	@Override
	    	public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
//                getGame().setScreen(new MenuScreen(getGame()));
	    		getGame().setScreen(ScreenManager.getScreen(MenuScreen.class.getName()));
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
	    
		Main.hero1.setBounds(400, Main.hero1.getY(), 250, 250);
		Main.hero2.setBounds(2, 2, 250, 250);
		
		Main.hero1.addListener(new ClickListener() {
			
			@Override
			public void clicked(InputEvent event, float x, float y) {
				// TODO Auto-generated method stub
//				super.clicked(event, x, y);
				
				Main.LOG.println("You clicked "+ Main.hero1.getName());
			}
			
		});
		
		this.addActor(Main.hero1);
		this.addActor(Main.hero2);
		this.addActor(title);
		this.addActor(playButton);
		

	}

	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		super.render(delta);
		Movement.DPadMovement(Main.hero1);
		Movement.WASDMovement(Main.hero2);
	}


}
