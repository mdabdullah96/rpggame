package com.saeedalam.chimera.actions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.saeedalam.chimera.settings.Character;

public class Movement {
	
	
	public static void DPadMovement(Character hero) {
		
		float heroSpeed = Gdx.graphics.getDeltaTime() * hero.getMovementSpeed();
		
//		final Rectangle bound = hero.getSprite().getBoundingRectangle();
		
//		if(bound.overlaps(Main.hero2.getSprite().getBoundingRectangle())) {
//			hero.attack(Main.hero2);
//		}
		
		if(Gdx.input.isKeyPressed(Keys.DPAD_LEFT) && hero.getX() >= 0 ) {
			hero.setX(hero.getX() - heroSpeed ); 
		}
		
		
		if(Gdx.input.isKeyPressed(Keys.DPAD_RIGHT) && hero.getX() <= Gdx.graphics.getBackBufferWidth() + hero.getWidth()  ) {
			hero.setX(hero.getX() + heroSpeed ); 
		} 
			   
		if(Gdx.input.isKeyPressed(Keys.DPAD_UP)) {

			hero.setY(hero.getY() + heroSpeed ); 
		}
		
		if(Gdx.input.isKeyPressed(Keys.DPAD_DOWN)) {

			hero.setY(hero.getY() - heroSpeed ); 
		}
		
	}
	
public static void WASDMovement(Character hero) {
		
		float heroSpeed = Gdx.graphics.getDeltaTime() * hero.getMovementSpeed();
		
//		final Rectangle bound = hero.getSprite().getBoundingRectangle();
		
		
//		if(bound.overlaps(Main.hero1.getSprite().getBoundingRectangle())) {
//			hero.attack(Main.hero1);
//		}
		
		if(Gdx.input.isKeyPressed(Keys.A) && hero.getX() >= 0 ) {
			hero.setX(hero.getX() - heroSpeed );  
		}
		
		if(Gdx.input.isKeyPressed(Keys.D) && hero.getX() <= Gdx.graphics.getBackBufferWidth() + hero.getWidth()  ) 
			hero.setX(hero.getX() + heroSpeed );    
		
		
		if(Gdx.input.isKeyPressed(Keys.W))
			hero.setY(hero.getY() + heroSpeed ); 
		
		if(Gdx.input.isKeyPressed(Keys.S))
			hero.setY(hero.getY() - heroSpeed ); 
		
	}
	

}
