package com.saeedalam.chimera.settings;

import java.util.ArrayList;

public class Hero extends Character {
	
	private int CharPoints = 0;
	private int GearPoints = 4;
	
	public Hero(String _name, int a, int d, int h, int hp, int level, Gear [] gears, String texture) {
		super(_name, a, d, h, hp, level, gears, texture);
		// TODO Auto-generated constructor stub
	}
	
	public Hero(String _name, int a, int d, int h, int hp, Gear [] gears, String texture) {
		super(_name, a, d, h, hp, gears, texture);
		// TODO Auto-generated constructor stub
	}
	
	public Hero(String _name, int a, int d, int h, Gear [] gears, String texture) {
		super(_name, a, d, h, gears, texture);
		// TODO Auto-generated constructor stub
	}

	public int getGearPoints() {
		return GearPoints;
	}

	public void setGearPoints(int gearPoints) {
		GearPoints = gearPoints;
	}

	public int getCharPoints() {
		return CharPoints;
	}

	public void setCharPoints(int charPoints) {
		CharPoints = charPoints;
	}
	
	public void useGearPoints(Gear gear) {
		Main.LOG.println(getGearPoints());
		gear.setAttack((gear.getAttack()+1));
		setGearPoints(getGearPoints() - 1);
		Main.LOG.println(getGearPoints());
	}
	
	public void useCharPoints(int i) {
		if(i == 0) setHealth(getHealth()+1);
		if(i == 1) setAttack(getAttack()+1);
		if(i == 2) setDefense(getDefense()+1);
		if(i == 3) setHeal(getHeal()+1);
	}
	
	
	@Override
	public void levelUp() {
		// TODO Auto-generated method stub
		
	}

	

}
