package com.saeedalam.chimera.settings;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.StringTokenizer;

import com.badlogic.gdx.Screen;
import com.saeedalam.chimera.screens.MenuScreen;


public class Main {

	public static PrintStream LOG = System.out;

	public static Gear w1 = new Gear(12, 3, 0, "axe", "axe.png");
	public static Gear w2 = new Gear(12, 5, 0, "hammer", "axe.png");
	
	

	public static Gear w3 = new Gear(26, 2, 0, "minigun", "semi_auto.png");
	public static Gear w4 = new Gear(5000, 2, 0, "shotgun", "shotgun.png");

	public static Gear w5 = new Gear(1, 1, 50, "elixir","healthPotion.png"); //heal
	public static Gear w6 = new Gear(1, 50, 1, "ruby", "bigShield.png"); //defence

	
	public static Gear [] gears = {w1, w2, w3, w4, w5};
	
	public static Hero hero1 = new Hero("saeed", 2000, 980, 17, 4000, gears, "saeed.png") ;
	public static Hero hero2 = new Hero("abdullah", 3800, 500, 0, 9999, gears, "dula.png");
	

	//setups the game
	public static void main(String [] args ) {
		
		hero1.equipGear(w3);
		hero1.equipGears(gears);
		
		hero1.setActiveGear(3);
		hero1.attack(hero2);
		
		Main.LOG.println("Class name for hero1: "+hero1.getClass().getName());
		
		ActiveObjects.addCharacters(hero1, hero2);
		ActiveObjects.addGears(w1, w2, w3, w4, w5);
		
//		hero2.attack(hero1);	
		
		String fileName = "assets/chimera_config.txt";
		
		String line = "";
		int lineCount = 0;
		
		try {

			FileReader fileReader = new FileReader(fileName);
			
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
			while((line = bufferedReader.readLine()) != null) {
//				LOG.println(line);
				StringTokenizer st = new StringTokenizer(line);
				while(st.hasMoreTokens()) {
					st.nextToken().matches("");
					LOG.println(st.nextToken());
					
				}
				lineCount++;
			}
			
			bufferedReader.close();
			
		}catch(FileNotFoundException e) {
			LOG.println(e.getMessage());
		}
		catch(IOException e) {
			LOG.println(e.getMessage());
		}
		
		
	}
}
