package com.saeedalam.chimera.settings;

/*
 * Actions are series of 'action' that a hero can make. Such as attack, heal etc
*/
public interface Actions {
	
	public void attack();
	
	public void defened();
	
	public void run();

}
