package com.saeedalam.chimera.settings;

import java.awt.List;
import java.util.ArrayList;
import java.util.Arrays;

import com.saeedalam.chimera.actions.Movement;

public abstract class Character extends BasicStats {
	

	private ArrayList<Gear> gears = new ArrayList<Gear>();
	private int health = 1000;
	private int level = 1;
	private int activeGear = 0; //index
	private int movementSpeed = 500;
	
	

	public Character(String _name, int a, int d, int h, int hp, int level, Gear [] gear, String texture) {

		super(_name, a, d, h, texture);
		
		for(Gear _gears: gear) {
			Main.LOG.println(_gears.getName());
		}
		
		this.health = hp; 
		this.level = level;

		this.gears.addAll(new ArrayList<>(Arrays.asList(gear)));
		this.setAttack(a + gear[0].getAttack());
		this.setDefense(d + gear[0].getDefense());
		this.setHeal(h + gear[0].getHeal());

	}
	
	public Character(String _name, int a, int d, int h, int hp, Gear [] gears, String texture) {
		super(_name, a, d, h, texture);
		this.health = hp; 

		this.gears.addAll(new ArrayList<>(Arrays.asList(gears)));
		
		this.setAttack(a + gears[0].getAttack());
		this.setDefense(d + gears[0].getDefense());
		this.setHeal(h + gears[0].getHeal());
	}
	
	public Character(String _name, int a, int d, int h, Gear [] gears, String texture) {
		super(_name, a, d, h, texture);
		

		this.gears.addAll(new ArrayList<>(Arrays.asList(gears)));
		this.setAttack(a + gears[0].getAttack());
		this.setDefense(d + gears[0].getDefense());
		this.setHeal(h + gears[0].getHeal());

	}
	
	public int getLevel() {
		return this.level;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	//this method is to add more attack, defense, heal points
	public abstract void levelUp();
	

	public int getHealth() {
		return this.health;
	}
	
	public void setHealth(int health) {
		this.health = health;
	}
	
	
	public int getMovementSpeed() {
		return movementSpeed;
	}

	public void setMovementSpeed(int movementSpeed) {
		this.movementSpeed = movementSpeed;
	}

	public void equipGear(Gear gear) {
		
		this.gears.add(gear);
		
	}
	
	public ArrayList<Gear> getGears() {
		return this.gears;
	}
	
	public void equipGears(Gear [] gear) {
		this.gears.addAll(new ArrayList<>(Arrays.asList(gear)));
	}
	
	public void bonus() {
		if(getAttack() > 5000) {
			
		}
	}

	public Gear getActiveGear() {
		return this.gears.get(activeGear);
	}

	public void setActiveGear(int activeGear) {
		this.activeGear = activeGear;
	}
	
	public void attack(Character hero) {
		Main.LOG.println(getName()+" started a fight with "+hero.getName());
		Main.LOG.println(getName()+" has active gear = "+ getActiveGear().getName());
		Main.LOG.println(hero.getName()+" has active gear = "+ hero.getActiveGear().getName());
		Main.LOG.printf("%1$-10s %2$-10s %3$-10s %4$-10s %5$-10s \n", "", "Gear Name", "Attack", "Defense", "Heal");
		Main.LOG.printf("%1$-10s %2$-10s %3$-10s %4$-10s %5$-10s \n", getName(), getActiveGear().getName(), getActiveGear().getAttack(), getActiveGear().getDefense(), getActiveGear().getHeal());
		Main.LOG.printf("%1$-10s %2$-10s %3$-10s %4$-10s %5$-10s \n", hero.getName(), hero.getActiveGear().getName(), hero.getActiveGear().getAttack(), hero.getActiveGear().getDefense(), hero.getActiveGear().getHeal());
		
		
		int currHeroTotalDefense = getDefense() + getActiveGear().getDefense();
		int currHeroTotalAttack = getAttack() + getActiveGear().getAttack();
//		int currHeroTotalHeal = getHeal() + getActiveGear().getHeal();
//		int currHeroTotalHealth = getHealth() + getActiveGear().getHeal();
		
		int otherHeroTotalDefense = hero.getDefense() + hero.getActiveGear().getDefense();
		int otehrHeroTotalAttack = hero.getAttack() + hero.getActiveGear().getAttack();
//		int otehrHeroTotalHeal = hero.getHeal() + hero.getActiveGear().getAttack();

		Main.LOG.printf("%1$-20s \n", "Stats of both heros: ");
		Main.LOG.printf("%1$-10s %2$-10s %3$-10s %4$-20s \n", "Heros: ", getName().toUpperCase(), "Vs",hero.getName().toUpperCase() );
		Main.LOG.printf("%1$-10s %2$-10s %3$-10s %4$-20s \n", "B Attack: ", getAttack(), "", hero.getAttack());
		Main.LOG.printf("%1$-10s %2$-10s %3$-10s %4$-20s \n", "Attack: ", currHeroTotalAttack, "", otehrHeroTotalAttack);
		Main.LOG.printf("%1$-10s %2$-10s %3$-10s %4$-20s \n", "Defense: ", currHeroTotalDefense, "", otherHeroTotalDefense);
		Main.LOG.printf("%1$-10s %2$-10s %3$-10s %4$-20s \n", "B Defense: ", getDefense(), "", hero.getDefense());
//		Main.LOG.printf("%1$-10s %2$-10s %3$-10s %4$-20s \n", "Heal: ", getHeal(), "", hero.getHeal() );
		Main.LOG.printf("%1$-10s %2$-10s %3$-10s %4$-20s \n", "Health: ", getHealth(), "", hero.getHealth() );
		
		
		if (otherHeroTotalDefense < currHeroTotalAttack) hero.setHealth((hero.getHealth() - (currHeroTotalAttack - otherHeroTotalDefense)));	

		if (hero.getHealth() < 0) hero.setHealth(0);
		
		Main.LOG.println("Health of " + hero.getName() + " is: " + hero.getHealth());
		Main.LOG.println("----------------------------------------------------");
	}
	

}
