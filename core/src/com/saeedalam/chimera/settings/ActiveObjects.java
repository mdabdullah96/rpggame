package com.saeedalam.chimera.settings;

import java.util.ArrayList;
import java.util.Arrays;

public class ActiveObjects {
	
	private static ArrayList<Character> character = new ArrayList<Character>();
	private static ArrayList<Gear> gears = new ArrayList<Gear>();
	
	public static void addCharacters(Character ... characters) {
		character.addAll(new ArrayList<>(Arrays.asList(characters)));
	}
	
	public static ArrayList<Character> getHeros(){
		return character;
	}

	public static ArrayList<Gear> getGears() {
		return gears;
	}

	public static void addGears(Gear ... _gears ) {
		gears.addAll(new ArrayList<>(Arrays.asList(_gears)));
	}

}
