package com.saeedalam.chimera.settings;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public abstract class BasicStats extends Actor{
	
	private String name = "";
	private int attack = 0;
	private int defense = 0;
	private int heal = 0;
	private int attackSpeed = 0;
	private Texture texture;
	private Sprite sprite;
	private TextureRegion textureRegion;
	
	public BasicStats(String name, int a, int d, int h, String aTexture) {
		this.name = name;
		this.attack = a;
		this.defense = d;
		this.heal = h;
		this.texture = new Texture(aTexture);
		this.sprite = new Sprite(texture);
		textureRegion = new TextureRegion(this.texture);
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String _name) {
		this.name = _name;
	}
	
	public int getAttackSpeed() {
		return this.attackSpeed;
	}
	
	public void setAttackSpeed(int speed) {
		this.attackSpeed = speed;
	}
	
	public int getAttack() {
		return this.attack;
	}
	
	public void setAttack(int a) {
		this.attack = a;
	}
	
	public int getDefense() {
		return this.defense;
	}
	
	public void setDefense(int d) {
		this.defense = d;
	}
	
	public int getHeal() {
		return this.heal;
	}
	
	public void setHeal(int h) {
		this.heal = h;
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		super.draw(batch, parentAlpha);
		batch.draw(textureRegion, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
	}
	
	public void setTexture(Texture texture) {
		this.texture = texture;
	}
	
	public Texture getTexture() {
		return this.texture;
	}
		
	public void setTextureRegion(TextureRegion textureRegion) {
		this.textureRegion = textureRegion;
	}
	
	public TextureRegion getTextureRegion() {
		return this.textureRegion;
	}
	
	public Sprite getSprite() {
		return sprite;
	}

	public void setSprite(Sprite sprite) {
		this.sprite = sprite;
	}
	
	@Override
	public void setX(float x) {
		// TODO Auto-generated method stub
		super.setX(x);
		this.getSprite().setX(x);
	}
	
	@Override
	public void setY(float y) {
		// TODO Auto-generated method stub
		super.setY(y);
		this.getSprite().setY(y);
	}
	
	@Override
	public void setBounds(float x, float y, float width, float height) {
		// TODO Auto-generated method stub
		super.setBounds(x, y, width, height);
		this.getSprite().setBounds(x, y, width, height);
	}

}
