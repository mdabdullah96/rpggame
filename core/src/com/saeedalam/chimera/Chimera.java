package com.saeedalam.chimera;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.saeedalam.chimera.screens.MenuScreen;
import com.saeedalam.chimera.screens.ScreenManager;

public class Chimera extends Game {
//	public static final int WIDTH = 1920;
//	public static final int HEIGHT = 1080;

	public static final int WIDTH = 800;
	public static final int HEIGHT = 800;
	
	public static final String TITLE = "Chimera Game";
	public static Skin gameSkin;
	
	Texture img;

	@Override
	public void create () {
		gameSkin = new Skin(Gdx.files.internal("pixthulhu/skin/pixthulhu-ui.json"));
		
		ScreenManager.initialize(this);
		this.setScreen(ScreenManager.getScreen(MenuScreen.class.getName()));

	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		super.dispose();
	}
}
