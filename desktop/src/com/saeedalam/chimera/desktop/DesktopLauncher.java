package com.saeedalam.chimera.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.saeedalam.chimera.Chimera;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.height = Chimera.HEIGHT;
		config.width = Chimera.WIDTH;
		config.title = Chimera.TITLE;
		
		new LwjglApplication(new Chimera(), config);
	}
}
